package com.gmail.val59000mc.uhctraining.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
