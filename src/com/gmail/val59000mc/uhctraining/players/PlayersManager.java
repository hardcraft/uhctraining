package com.gmail.val59000mc.uhctraining.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.uhctraining.classes.PlayerClassManager;
import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.dependencies.VaultManager;
import com.gmail.val59000mc.uhctraining.game.EndCause;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.game.GameState;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.uhctraining.titles.TitleManager;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<UPlayer> players;
	private List<UTeam> teams;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<UPlayer>());
		teams = Collections.synchronizedList(new ArrayList<UTeam>());
		
		teams.add(new UTeam(TeamType.RED, Config.spawnsLocations.get(0)));
		teams.add(new UTeam(TeamType.BLUE, Config.spawnsLocations.get(1)));
		teams.add(new UTeam(TeamType.YELLOW, Config.spawnsLocations.get(2)));
		teams.add(new UTeam(TeamType.GOLD, Config.spawnsLocations.get(3)));
		teams.add(new UTeam(TeamType.GREEN, Config.spawnsLocations.get(4)));
		teams.add(new UTeam(TeamType.DARK_PURPLE, Config.spawnsLocations.get(5)));
		teams.add(new UTeam(TeamType.LIGHT_PURPLE, Config.spawnsLocations.get(6)));
		teams.add(new UTeam(TeamType.AQUA, Config.spawnsLocations.get(7)));
		
	}
	
	
	// Accessors
	
	public UPlayer getUPlayer(Player player){
		return getUPlayer(player.getName());
	}
	
	public UPlayer getUPlayer(String name){
		for(UPlayer gPlayer : getPlayers()){
			if(gPlayer.getName().equals(name))
				return gPlayer;
		}
		
		return null;
	}
	
	public synchronized List<UPlayer> getPlayers(){
		return players;
	}
	
	public synchronized List<UTeam> getTeams(){
		return teams;
	}


	
	public synchronized UTeam getTeam(TeamType type){
		for(UTeam team : getTeams()){
			if(team.getType().equals(type)){
				return team;
			}
		}
		return null;
	}
	
	private UTeam getMaxNotFullTeam(){
		UTeam max = null;
		for(UTeam team : getTeams()){
			if(!team.isFull()){
				if(max == null){
					max = team;
				}else{
					if(team.getMembers().size() > max.getMembers().size()){
						max = team;
					}
				}
			}
		}
		return max;
	}

	public List<UTeam> getPlayingTeams() {

		List<UTeam> playingTeams = new ArrayList<UTeam>();
		for(UTeam slaveTeam : getTeams()){
			if(slaveTeam.isPlaying()){
				playingTeams.add(slaveTeam);
			}
		}
		
		return playingTeams;
	}
	
	
	// Methods 
	
	public synchronized UPlayer addPlayer(Player player){
		UPlayer newPlayer = new UPlayer(player);
		getPlayers().add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		UPlayer gPlayer = getUPlayer(name);
		if(gPlayer != null){
			gPlayer.leaveTeam();
			getPlayers().remove(gPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		UPlayer gPlayer = getUPlayer(player);
		if(gPlayer != null){
			return gPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public boolean isPlayerAllowedToJoin(Player player){
		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
		GameManager gm = GameManager.instance();
		
		switch(gm.getState()){
				
			case WAITING:
			case PLAYING:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
				return true;
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
				return false;
		}
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		UPlayer gPlayer = getUPlayer(player);
		
		if(gPlayer == null){
			gPlayer = addPlayer(player);
		}
		
		GameState gameState = GameManager.instance().getState();
		switch(gameState){
			case WAITING:
				gPlayer.setState(PlayerState.WAITING);
				break;
			case PLAYING:
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				gPlayer.setState(PlayerState.DEAD);
				break;
		}
			
		switch(gPlayer.getState()){
			case WAITING:
				Logger.debug("waitPlayer");
				gPlayer.sendI18nMessage("player.welcome");
				Logger.broadcast(
					I18n.get("player.joined")
						.replace("%player%",gPlayer.getName())
						.replace("%count%", String.valueOf(getPlayers().size()))
						.replace("%total%",  String.valueOf(Config.maxPlayers))
				);
				if(Config.isBountifulApiLoaded){
					TitleManager.sendTitle(player, ChatColor.GREEN+"UHC Training", 20, 20, 20);
				}
				waitPlayer(gPlayer);
				break;
			case DEAD:
			default:
				Logger.debug("spectatePlayer");
				spectatePlayer(gPlayer);
				break;
		}

		
		gPlayer.refreshNameTag();
		refreshVisiblePlayers();
	}

	public void waitPlayer(UPlayer gPlayer){
		
		if(gPlayer.isOnline()){
			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
				gPlayer.sendI18nMessage("player.full");
			}
			Player player = gPlayer.getPlayer();
			player.teleport(Config.lobby);
			player.setGameMode(GameMode.ADVENTURE);
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 1);
			player.setHealth(20);

			if(Config.isSIGLoaded){
				SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
			}
		}
		
	}
	
	public void startPlayer(final UPlayer uPlayer){
		uPlayer.setState(PlayerState.PLAYING);
		uPlayer.teleportToSpawnPoint();
		uPlayer.setGlobalChat(false);
		UpdateScoreboardThread.add(uPlayer);
		if(uPlayer.isOnline()){
			Player player = uPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			player.setFireTicks(0);
			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 2, 2);
			Effects.clear(player);
			PlayerClassManager.instance().giveClassItems(uPlayer);
		}
	}
	
	public void spectatePlayer(UPlayer gPlayer){

		UpdateScoreboardThread.add(gPlayer);
		gPlayer.setState(PlayerState.DEAD);
		gPlayer.sendI18nMessage("player.spectate");
		
		if(gPlayer.isOnline()){
			Player player = gPlayer.getPlayer();
			Inventories.clear(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(Config.lobby);
		}
	}
	
	public List<UPlayer> getPlayingPlayers() {
		List<UPlayer> playingPlayers = new ArrayList<UPlayer>();
		for(UPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}
	
	public List<UPlayer> getWaitingPlayers() {
		List<UPlayer> waitingPlayers = new ArrayList<UPlayer>();
		for(UPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}

	public void startAllPlayers() {
		Logger.debug("-> PlayersManager::startAllPlayers");
		assignRandomTeamsToPlayers();
		refreshVisiblePlayers();
		for(UPlayer gPlayer : getPlayers()){
			if(gPlayer.getTeam() == null){
				spectatePlayer(gPlayer);
			}else{
				startPlayer(gPlayer);
			}
		}
		UpdateScoreboardThread.start();
		Logger.debug("<- PlayersManager::startAllPlayers");
	}
	
	public void refreshVisiblePlayers() {
		for(Player player : Bukkit.getOnlinePlayers()){
			for(Player onePlayer : Bukkit.getOnlinePlayers()){
				player.hidePlayer(onePlayer);
				player.showPlayer(onePlayer);
			}
		}
	}
	
	private void assignRandomTeamsToPlayers(){
		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
		
		synchronized(players){
			// Shuffle players who will play (up to maxPlayers)
			List<UPlayer> playersWhoWillPlay = new ArrayList<UPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
			List<UPlayer> playersWhoWillNotPlay = new ArrayList<UPlayer>();
			if(players.size() > Config.maxPlayers){
				playersWhoWillNotPlay = new ArrayList<UPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
			}
			this.players = new ArrayList<UPlayer>();
			Collections.shuffle(playersWhoWillPlay);
			this.players.addAll(playersWhoWillPlay);
			this.players.addAll(playersWhoWillNotPlay);

			for(UPlayer evoPlayer : playersWhoWillNotPlay){
				evoPlayer.leaveTeam();
			}
			
		}
		
		// assign two players by team
		
		int nPlayer = 0;
		for(UPlayer evoPlayer : getWaitingPlayers()){
			nPlayer++;
			
			if(nPlayer <= Config.maxPlayers){
				
				UTeam team = evoPlayer.getTeam();
				
				if(evoPlayer.getTeam() == null){
					team = getMaxNotFullTeam();
					team.addPlayer(evoPlayer);
				}
				
			}
			
			evoPlayer.refreshNameTag();
		}

		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
	}

	public void endAllPlayers(EndCause cause, UTeam winningTeam) {
		
		if(winningTeam != null){
			for(UPlayer gPlayer : winningTeam.getMembers()){
				if(gPlayer.isOnline()){
					rewardWin(gPlayer);
				}
			}
		}

		for(UPlayer gPlayer : getPlayers()){
			if(gPlayer.isOnline()){
				gPlayer.setGlobalChat(true);
				spectatePlayer(gPlayer);
				printEndMessage(gPlayer,winningTeam);
			}
		}

		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
		
	}

	private void rewardWin(UPlayer gPlayer) {
		if(gPlayer.isOnline()){
			Player player = gPlayer.getPlayer();
			double money = VaultManager.addMoney(player, Config.winReward);
			gPlayer.addMoney(money);
			player.sendMessage("§a+"+money+" §fHC");
			Sounds.play(player, Sound.LEVEL_UP, 1, 2);
		}
	}
	
	private void printEndMessage(UPlayer rPlayer, UTeam winningTeam){
		if(rPlayer.isOnline() && winningTeam != null){
			Player player = rPlayer.getPlayer();
			Texts.tellraw(player, I18n.get("stats.end",player)
					.replace("%kills%", String.valueOf(rPlayer.getKills()))
					.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
					.replace("%winner%", winningTeam.getColor()+winningTeam.getI18nName(player))
			);
		}
	}
	
	
}
