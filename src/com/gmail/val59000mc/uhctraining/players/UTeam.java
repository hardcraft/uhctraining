package com.gmail.val59000mc.uhctraining.players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.i18n.I18n;

public class UTeam {

	private List<UPlayer> members;
	private TeamType type;
	private Location spawnPoint;
	
	public UTeam(TeamType type,Location spawnPoint) {
		this.members = new ArrayList<UPlayer>();
		this.spawnPoint = spawnPoint;
		this.type = type;
	}
	public ChatColor getColor() {
		return getType().getColor();
	}

	public TeamType getType() {
		return type;
	}
	public void sendI18nMessage(String code) {
		for(UPlayer wicPlayer: members){
			wicPlayer.sendI18nMessage(code);
		}
	}
	
	public String getI18nName(Player player){
		return I18n.get("team."+getType().toString(), player);
	}
	
	public boolean contains(UPlayer player){
		return members.contains(player);
	}
	
	public synchronized List<UPlayer> getMembers(){
		return members;
	}

	public List<UPlayer> getPlayingMembers(){
		List<UPlayer> playingMembers = new ArrayList<UPlayer>();
		for(UPlayer uhcPlayer : getMembers()){
			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
				playingMembers.add(uhcPlayer);
			}
		}
		return playingMembers;
	}
	
	public synchronized List<String> getMembersNames(){
		List<String> names = new ArrayList<String>();
		for(UPlayer player : getMembers()){
			names.add(player.getName());
		}
		return names;
	}
	
	public void addPlayer(UPlayer gPlayer){
		gPlayer.leaveTeam();
		gPlayer.setTeam(this);
		getMembers().add(gPlayer);
		gPlayer.setSpawnPoint(this.spawnPoint);
	}

	public boolean isFull(){
		return getMembers().size() >= Config.playersPerTeam;
	}
	
	public void leave(UPlayer gPlayer){
		getMembers().remove(gPlayer);
		gPlayer.setSpawnPoint(Config.lobby);
	}
	
	public boolean isOnline(){
		for(UPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline()){
				return true;
			}
		}
		return false;
	}


	public boolean isPlaying() {
		for(UPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline() && gPlayer.isState(PlayerState.PLAYING)){
				return true;
			}
		}
		return false;
	}
	
	public List<UPlayer> getOtherMembers(UPlayer excludedPlayer){
		List<UPlayer> otherMembers = new ArrayList<UPlayer>();
		for(UPlayer uhcPlayer : getMembers()){
			if(!uhcPlayer.equals(excludedPlayer))
				otherMembers.add(uhcPlayer);
		}
		return otherMembers;
	}

}
