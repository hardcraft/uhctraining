package com.gmail.val59000mc.uhctraining.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.gmail.val59000mc.uhctraining.players.UTeam;

public class InteractListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRightClickItem(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack hand = player.getItemInHand();
		

		if(hand.getType().equals(Material.SKULL_ITEM) 
			&& (event.getAction() == Action.RIGHT_CLICK_AIR 
				|| event.getAction() == Action.RIGHT_CLICK_BLOCK)){
			

			UPlayer uPlayer = PlayersManager.instance().getUPlayer(player);
			UTeam uTeam = uPlayer.getTeam();
			if(uPlayer != null && uTeam != null){
				uTeam.sendI18nMessage("items.regenerating-team");
				for(UPlayer uP : uTeam.getPlayingMembers()){
					Effects.add(uP.getPlayer(), PotionEffectType.REGENERATION, 100, 1);
				}
				event.setCancelled(true);
				player.getInventory().remove(hand);
			}
		}
		
		
		
	}
}
