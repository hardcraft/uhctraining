package com.gmail.val59000mc.uhctraining.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.dependencies.VaultManager;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.game.GameState;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.players.PlayerState;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.google.common.collect.Lists;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		UPlayer wicPlayer = PlayersManager.instance().getUPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, UPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			
			UPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getUPlayer(dead.getPlayer().getKiller());
			
			event.setDeathMessage(I18n.get("player.died").replace("%player%",dead.getColor()+dead.getName()));
			
			if(killer != null && killer.isOnline()){
				killer.addKill();
				rewardKill(killer);
				
				event.setDeathMessage(
						I18n.get("player.killed")
						.replace("%killer%",killer.getColor()+killer.getName())
						.replace("%killed%",dead.getColor()+dead.getName())
				);
			}
			
			event.getDrops().add(getRegenHead(dead.getName()));
			
			lightiningEffect(event.getEntity().getLocation());
			
			dead.setState(PlayerState.DEAD);
			
			if(dead.isOnline() && PlayersManager.instance().getPlayingTeams().size() > 1){

				Texts.tellraw(dead.getPlayer(), I18n.get("stats.death",dead.getPlayer())
					.replace("%kills%", String.valueOf(dead.getKills()))
					.replace("%hardcoins%", String.valueOf(dead.getMoney()))
				);
				
			}
			
			Bukkit.getScheduler().runTaskLater(UHCTraining.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);
		}
		
	}
	
	private void lightiningEffect(Location location) {
		location.getWorld().strikeLightningEffect(location);
	}

	private ItemStack getRegenHead(String playerName) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setDisplayName("§c"+playerName);
		meta.setOwner(playerName);
		meta.setLore(Lists.newArrayList(I18n.get("items.regen-head-lore")));
		head.setItemMeta(meta);
		return head;
	}

	private void rewardKill(UPlayer killer) {
		if(killer.isOnline()){
			Player player = killer.getPlayer();
			double money = VaultManager.addMoney(player, Config.killReward);
			killer.addMoney(money);
			player.sendMessage("§a+"+money+" §fHC");
			Sounds.play(player, Sound.LEVEL_UP, 1, 2);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		UPlayer wicPlayer = PlayersManager.instance().getUPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			return;
		}
		
		handleRespawnPlayer(event,wicPlayer);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, UPlayer wicPlayer){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+wicPlayer);

		final String name = wicPlayer.getName();
		
		switch(GameManager.instance().getState()){
			case WAITING:
					Bukkit.getScheduler().runTaskLater(UHCTraining.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("waitPlayer");
						UPlayer p = PlayersManager.instance().getUPlayer(name);
						if(p != null){
							PlayersManager.instance().waitPlayer(p);
						}
						
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
			case PLAYING:
				Bukkit.getScheduler().runTaskLater(UHCTraining.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("teamSpawnPlayer");
						UPlayer p = PlayersManager.instance().getUPlayer(name);
						if(p != null){
							PlayersManager.instance().spectatePlayer(p);
						}
						
					}
				}, 1);
				break;
		}
		

		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
	}
	
}
