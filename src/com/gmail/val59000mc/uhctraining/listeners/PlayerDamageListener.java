package com.gmail.val59000mc.uhctraining.listeners;

import java.util.Collection;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.gmail.val59000mc.uhctraining.players.PlayerState;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;

public class PlayerDamageListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handleFriendlyFire(event);
		handleArrow(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}


	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPotionSplash(PotionSplashEvent event){
		handlePotionSplash(event);
	}
	
	
	///////////////////////
	// PotionSplashEvent //
	///////////////////////
	
	private void handlePotionSplash(PotionSplashEvent event) {
		if(event.getEntity().getShooter() instanceof Player){
			PlayersManager pm = PlayersManager.instance();
			
			Player damager = (Player) event.getEntity().getShooter();
			UPlayer tDamager = pm.getUPlayer(damager);
			
			if(tDamager != null){
			
				if(isAttackPotion(event.getPotion())){
					// Cancelling potion damage for teamates
					for(LivingEntity living : event.getAffectedEntities()){
						if(living instanceof Player){
							Player damaged = (Player) living;
							UPlayer tDamaged = pm.getUPlayer(damaged);
							if(tDamager.isInTeamWith(tDamaged)){
								event.setIntensity(living, 0);
							}
						}
					}
				}
				
			}
		}
	
	}
	
	// Only checking the first potion effect, considering vanilla potions
	private boolean isAttackPotion(ThrownPotion potion){
		Collection<PotionEffect> effects = potion.getEffects();
		if(effects.size() > 0){
			PotionEffectType effect = effects.iterator().next().getType();
			return ( 
				effect.equals(PotionEffectType.HARM) ||
			    effect.equals(PotionEffectType.POISON) ||
			    effect.equals(PotionEffectType.WEAKNESS) ||
				effect.equals(PotionEffectType.SLOW) ||
			    effect.equals(PotionEffectType.SLOW_DIGGING) ||
			    effect.equals(PotionEffectType.CONFUSION) ||
			    effect.equals(PotionEffectType.BLINDNESS) ||
			    effect.equals(PotionEffectType.HUNGER) ||
			    effect.equals(PotionEffectType.WITHER)
			  );
		}
		return false;
	}

	
	///////////////////////
	// EntityDamageEvent //
	///////////////////////
	
	private void handleAnyDamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
			UPlayer wicPlayer = PlayersManager.instance().getUPlayer((Player) event.getEntity());
			if(wicPlayer != null && wicPlayer.getState().equals(PlayerState.WAITING)){
				event.setCancelled(true);
			}
		}
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////
	
	private void handleFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			Logger.debug(damager.getName()+" tried to hit "+damaged.getName());
			
			UPlayer wicDamager = pm.getUPlayer(damager);
			UPlayer wicDamaged = pm.getUPlayer(damaged);
			
			if(wicDamaged != null && wicDamager != null){
				if(wicDamaged.isInTeamWith(wicDamager)){
					event.setCancelled(true);
				}
			}
			Logger.debug(event.isCancelled() ? "Hit cancelled " : "Hit successful");
		}
	}

	private void handleArrow(EntityDamageByEntityEvent event){

		if(!event.isCancelled()){
			PlayersManager pm = PlayersManager.instance();
			GameManager gm = GameManager.instance();
			
			
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Arrow){
				Projectile arrow = (Projectile) event.getDamager();
				final Player shot = (Player) event.getEntity();
				if(arrow.getShooter() instanceof Player){
					
					if(!gm.isPvp()){
						event.setCancelled(true);
						return;
					}
					
					final Player shooter = (Player) arrow.getShooter();
					UPlayer wicDamager = pm.getUPlayer(shooter);
					UPlayer wicDamaged = pm.getUPlayer(shot);

					if(wicDamager != null && wicDamaged != null){
						if(wicDamager.getState().equals(PlayerState.PLAYING) && wicDamager.isInTeamWith(wicDamaged)){
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
	}
}
