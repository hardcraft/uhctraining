package com.gmail.val59000mc.uhctraining.threads;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;

public class PreventMoveThread implements Runnable {
	
	private static PreventMoveThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> PreventMoveThread::start");
		PreventMoveThread thread = new PreventMoveThread();
		Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), thread);
		Logger.debug("<- PreventMoveThread::start");
	}
	
	private PreventMoveThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				if(run){
						
					for(Player player : Bukkit.getOnlinePlayers()){

						UPlayer gPlayer = PlayersManager.instance().getUPlayer(player);
						
						if(gPlayer != null && gPlayer.isOnline() && gPlayer.getSpawnPoint() != null){
							
							Location playerLocation = player.getLocation();
							Location lockLocation = gPlayer.getSpawnPoint();
							
							double xPlayer = playerLocation.getX();
							double zPlayer = playerLocation.getZ();
							double xLock = lockLocation.getX();
							double zLock = lockLocation.getZ();
							
							if(xPlayer != xLock || zPlayer != zLock){
								Location loc = lockLocation.clone();
								loc.setYaw(playerLocation.getYaw());
								loc.setPitch(playerLocation.getPitch());
								player.teleport(loc);
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), thread, 10);
				}
				
				
			}
			
		});
		
	}

}
