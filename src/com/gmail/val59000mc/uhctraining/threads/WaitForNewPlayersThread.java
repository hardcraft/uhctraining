package com.gmail.val59000mc.uhctraining.threads;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.titles.TitleManager;

public class WaitForNewPlayersThread implements Runnable{

	private static WaitForNewPlayersThread instance;
	
	private int remainingTime;
	private boolean willStart;
	private SimpleScoreboard scoreboard;
	
	
	public static void start(){
		Logger.debug("-> WaitForNewPlayersThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), new WaitForNewPlayersThread());
		}
		Logger.debug("<- WaitForNewPlayersThread::start");
	}

	public static boolean force() {
		if(instance != null){
			instance.willStart = true;
			instance.remainingTime = 10;
			return true;
		}
		return false;
	}
	
	public WaitForNewPlayersThread(){
		instance = this;
		this.remainingTime = Config.countdownToStart;
		this.willStart = false;
		this.scoreboard = new SimpleScoreboard("UHC Training");
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				int onlinePlayers = Bukkit.getOnlinePlayers().size();
				
				// update scoreboard
				updateScoreboard();
				
				if(willStart || onlinePlayers >= Config.playersToStart){
					willStart = true;
					if(remainingTime <= 0){
						GameManager.instance().startGame();
						return;
					}
					if(remainingTime <= 10 || (remainingTime > 0 && remainingTime%10 == 0 ) || remainingTime == Config.countdownToStart){
						Logger.info(I18n.get("game.starting-in").replace("%time%", Time.getFormattedTime(remainingTime)));
						
						if(Config.isBountifulApiLoaded){
							for(Player p : Bukkit.getOnlinePlayers()){
								if(remainingTime > 5){
									TitleManager.sendTitle(p, "§a"+remainingTime, 0, 20, 20);
								}else{
									TitleManager.sendTitle(p, "§c"+remainingTime, I18n.get("player.please-enchant",p), 0, 30, 0);
								}
							}
						}
						
						Sounds.playAll(Sound.NOTE_PIANO);
					}
					remainingTime--;
				}else{
					instance.remainingTime = Config.countdownToStart;
				}
				
				
				Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), instance, 20);
			
			}
		
		});

	}
	
	private void updateScoreboard(){
		
		List<String> content = new ArrayList<String>();
		content.add(" ");
		
		// Team
		content.add(I18n.get("scoreboard.online-players"));
		content.add(" "+ChatColor.GREEN+Bukkit.getOnlinePlayers().size()+" / "+Config.maxPlayers);
		
		if(remainingTime != Config.countdownToStart){
			// Kills / Deaths
			content.add(I18n.get("scoreboard.time-to-start"));
			content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingTime));
		}
		
		
		scoreboard.clear();
		for(String line : content){
			scoreboard.add(line);
		}
		scoreboard.draw();
		
		for(Player player : Bukkit.getOnlinePlayers()){
			scoreboard.send(player);
		}
		
		
	}
	
}