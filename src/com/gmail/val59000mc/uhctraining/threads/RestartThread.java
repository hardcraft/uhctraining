package com.gmail.val59000mc.uhctraining.threads;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.configuration.Config;

public class RestartThread implements Runnable{

	private static RestartThread instance;
	
	RestartThread thread;
	long remainingTime;
	
	
	public static void start(){
		Logger.debug("-> RestartThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), new RestartThread());
		}
		Logger.debug("<- RestartThread::start");
	}
	
	
	public RestartThread(){
		instance = this;
		this.remainingTime = 15;
	}
	
	@Override
	public void run(){
		
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				
				remainingTime--;
				
				if(remainingTime == 5){
					if(Config.isBungeeEnabled){
						for(Player player : Bukkit.getOnlinePlayers()){
							Bungee.sendPlayerToServer(UHCTraining.getPlugin(),player,Config.bungeeServer);
						}
					}
				}
				
				if(remainingTime <= 0){
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
				}else{
					Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), instance, 20);
				}
			}
		});		
		
	}

}
