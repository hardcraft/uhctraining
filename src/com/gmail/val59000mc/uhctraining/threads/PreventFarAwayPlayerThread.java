package com.gmail.val59000mc.uhctraining.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.game.GameState;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.UPlayer;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	private LocationBounds bounds;
	
	public static void start(LocationBounds bounds){
		Logger.debug("-> KillFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread(bounds);
		Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), thread);
		Logger.debug("<- KillFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread(LocationBounds bounds) {
		thread = this;
		this.run = true;
		this.bounds = bounds;
	}
	
	public static void stop() {
		thread.run = false;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				if(run){
					
					if(bounds != null && bounds.getWorld() != null){

						World world = bounds.getWorld();
						
						for(Player player : Bukkit.getOnlinePlayers()){

							UPlayer wicPlayer = PlayersManager.instance().getUPlayer(player);
							
							if(wicPlayer != null && player.getWorld().equals(world)){
								
								if(!bounds.contains(player.getLocation())){
									
									if( !GameManager.instance().isState(GameState.PLAYING) || player.getLocation().getY() >= 0){
										if(wicPlayer.isPlaying() && wicPlayer.getTeam() != null){
											if(player.getHealth() > 0){
												player.setHealth(0);
												Sounds.play(player, Sound.HURT_FLESH, 1, 1);
											}
										}else{
											player.teleport(Config.lobby);
										}
										wicPlayer.sendI18nMessage("game.leave-arena");
									}else if(GameManager.instance().isState(GameState.PLAYING) 
											&& wicPlayer.isPlaying()
											&& player.getLocation().getY() < 0 
											&& player.getHealth() > 0){
										player.setHealth(0);
										Sounds.play(player, Sound.HURT_FLESH, 1, 1);
									}
									
								}
								
								
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), thread, 50);
				}
				
				
			}
			
		});
		
	}

}
