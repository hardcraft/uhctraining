package com.gmail.val59000mc.uhctraining.threads;

import java.util.List;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.game.EndCause;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.players.UTeam;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;


public class CheckRemainingPlayerThread implements Runnable{

	private static CheckRemainingPlayerThread instance;
	
	private boolean run;
	
	public static void start(){
		Logger.debug("-> CheckRemainingPlayerThread::start");
		CheckRemainingPlayerThread thread = new CheckRemainingPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), thread);
		Logger.debug("<- CheckRemainingPlayerThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public CheckRemainingPlayerThread(){
		instance = this;
		this.run = true;
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable(){

			@Override
			public void run(){
				if(run){
					List<UTeam> playingTeams = PlayersManager.instance().getPlayingTeams();
					if(playingTeams.size() == 1){
						GameManager.instance().endGame(EndCause.TEAM_WIN,playingTeams.get(0));
					}
					if(playingTeams.size() == 0){
						GameManager.instance().endGame(EndCause.NO_MORE_PLAYERS,null);
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), instance, 20);
				}
			}
			
		});

	}
}
