package com.gmail.val59000mc.uhctraining.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.players.PlayerState;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.UPlayer;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<UPlayer,SimpleScoreboard> scoreboards;
	


	public static void start(){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		Bukkit.getScheduler().runTaskAsynchronously(UHCTraining.getPlugin(), instance);
	}
	
	public static void add(UPlayer gPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addGPlayer(gPlayer);
	}
	
	private void addGPlayer(UPlayer gPlayer){
		if(!scoreboards.containsKey(gPlayer)){
			SimpleScoreboard sc = new SimpleScoreboard("UHC Training");
			if(gPlayer.isOnline()){
				setupTeamColors(sc,gPlayer);
			}
			getScoreboards().put(gPlayer, sc);
		}
	}
	
	private void setupTeamColors(SimpleScoreboard sc, UPlayer gPlayer) {
		PlayersManager pm = PlayersManager.instance();
		
		Scoreboard scoreboard = sc.getBukkitScoreboard();
		Objective life = scoreboard.registerNewObjective(ChatColor.DARK_RED+"\u2764", "health");
		life.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		
		// Putting players in colored teams
		for(UPlayer aGPlayer : pm.getPlayers()){
				
			if(aGPlayer.isOnline()){
				life.getScore(aGPlayer.getName()).setScore((int) Math.round(aGPlayer.getPlayer().getHealth()));
			}
		}
		
		if(gPlayer.isOnline()){
			gPlayer.getPlayer().setScoreboard(scoreboard);
		}
		
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<UPlayer,SimpleScoreboard>());
	}
	
	private synchronized Map<UPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	@Override
	public void run() {
		Bukkit.getScheduler().runTask(UHCTraining.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				
						
					for(Entry<UPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
						UPlayer gPlayer = entry.getKey();
						SimpleScoreboard scoreboard = entry.getValue();
						
						if(gPlayer.isOnline() && gPlayer.getTeam() != null){

							Player player = gPlayer.getPlayer();
							
							List<String> content = new ArrayList<String>();
							content.add(" ");
							
							// Kills / Deaths
							content.add(I18n.get("scoreboard.kills",player));
							content.add(" "+ChatColor.GREEN+""+gPlayer.getKills()+ChatColor.WHITE);
							
							// Coins
							content.add(I18n.get("scoreboard.coins-earned",player));
							content.add(" "+ChatColor.GREEN+""+gPlayer.getMoney());
							
							// Teammate
							content.add(I18n.get("scoreboard.teammates",player));
							for(UPlayer gTeamMate : gPlayer.getTeam().getMembers()){
								if(gTeamMate.isState(PlayerState.PLAYING)){
									content.add(" "+gPlayer.getTeam().getColor()+""+gTeamMate.getName());
								}else{
									content.add(" "+gPlayer.getTeam().getColor()+""+gTeamMate.getName());
								}
							}								
							
							scoreboard.clear();
							for(String line : content){
								scoreboard.add(line);
							}
							scoreboard.draw();
							scoreboard.send(player);
						}
					}

					Bukkit.getScheduler().runTaskLaterAsynchronously(UHCTraining.getPlugin(), instance, 20);
				}
				
			});
				
		
	}
	
	
	
	
}
