package com.gmail.val59000mc.uhctraining.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.uhctraining.players.PlayersManager;

public class HcDebugCommand implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			if(args.length > 0){
				switch(args[0]){
				case "visibility":
					if(args.length == 2){
						handleDebugVisibility((Player) sender, args[1]);
					}else{
						sender.sendMessage("§cType /hcdebug visibility <player/all>");
					}
				}
			}
			
		}
		return true;
	}

	private void handleDebugVisibility(Player sender, String arg) {
		if(arg.equals("all")){
			PlayersManager.instance().refreshVisiblePlayers();
			sender.sendMessage("§aAll player had their visibility refreshed");
		}else{
			Player player = Bukkit.getPlayer(arg);
			if(player == null){
				sender.sendMessage("§c"+arg+" is not online");
			}else{
				for(Player p : Bukkit.getOnlinePlayers()){
					p.hidePlayer(player);
					p.showPlayer(player);
				}
				sender.sendMessage("§a"+arg+" got his visibility refreshed for everyone");
			}
		}
	}
}
