package com.gmail.val59000mc.uhctraining.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.classes.PlayerClassManager;
import com.gmail.val59000mc.uhctraining.commands.ChatCommand;
import com.gmail.val59000mc.uhctraining.commands.HcDebugCommand;
import com.gmail.val59000mc.uhctraining.commands.StartCommand;
import com.gmail.val59000mc.uhctraining.configuration.Config;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.listeners.InteractListener;
import com.gmail.val59000mc.uhctraining.listeners.PingListener;
import com.gmail.val59000mc.uhctraining.listeners.PlayerChatListener;
import com.gmail.val59000mc.uhctraining.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.uhctraining.listeners.PlayerDamageListener;
import com.gmail.val59000mc.uhctraining.listeners.PlayerDeathListener;
import com.gmail.val59000mc.uhctraining.maploader.MapLoader;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.UTeam;
import com.gmail.val59000mc.uhctraining.threads.CheckRemainingPlayerThread;
import com.gmail.val59000mc.uhctraining.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.uhctraining.threads.RestartThread;
import com.gmail.val59000mc.uhctraining.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.uhctraining.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.uhctraining.titles.TitleManager;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	private boolean pvp;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
		this.pvp = false;
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}
	
	

	public boolean isPvp() {
		return pvp;
	}


	public boolean isState(GameState state) {
		return getState().equals(state);
	}
	
	// methods 
	
	public void loadGame() {
		state = GameState.LOADING;
		Logger.debug("-> GameManager::loadNewGame");
		Config.load();
		PlayerClassManager.load();
		
		MapLoader.deleteOldPlayersFiles();
		MapLoader.load();
		
		registerCommands();
		registerListeners();
		
		if(Config.isBungeeEnabled)
			UHCTraining.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(UHCTraining.getPlugin(), "BungeeCord");

		waitForNewPlayers();
		Logger.debug("<- GameManager::loadNewGame");
	}


	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDamageListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new InteractListener());
		listeners.add(new PingListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, UHCTraining.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners	
		UHCTraining.getPlugin().getCommand("chat").setExecutor(new ChatCommand());
		UHCTraining.getPlugin().getCommand("start").setExecutor(new StartCommand());
		UHCTraining.getPlugin().getCommand("hcdebug").setExecutor(new HcDebugCommand());
		Logger.debug("<- GameManager::registerCommands");
	}



	private void waitForNewPlayers(){
		Logger.debug("-> GameManager::waitForNewPlayers");
		WaitForNewPlayersThread.start();
		PreventFarAwayPlayerThread.start(Config.lobbyBounds);
		UpdateScoreboardThread.start();
		state = GameState.WAITING;
		Logger.infoC(I18n.get("game.player-allowed-to-join"));
		Logger.debug("<- GameManager::waitForNewPlayers");
	}
	
	public void startGame(){
		Logger.debug("-> GameManager::startGame");
		state = GameState.STARTING;
		
		Logger.info(I18n.get("game.start"));
		
		if(Config.isBountifulApiLoaded){
			for(Player p : Bukkit.getOnlinePlayers()){
				TitleManager.sendTitle(p, I18n.get("game.start",p), I18n.get("player.please-enchant",p), 0, 40, 20);
			}
		}
		
		PreventFarAwayPlayerThread.stop();
		

		// Setup border
		WorldBorder wb = Bukkit.getWorld(Config.worldName).getWorldBorder();
		wb.setSize(10000);
		wb.setCenter(Config.borderCenter);
		
		
		PlayersManager.instance().startAllPlayers();	
		

		wb.setCenter(Config.borderCenter);
		wb.setSize(Config.borderStart);
		wb.setSize(Config.borderEnd,Config.borderTime);
		Config.lobby = Config.borderCenter;
		
		
		playGame();
		Logger.debug("<- GameManager::startGame");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		pvp = true;
		CheckRemainingPlayerThread.start();
		PreventFarAwayPlayerThread.start(Config.bounds);
	}
	
	public void endGame(EndCause cause, UTeam winningTeam) {
		if(state.equals(GameState.PLAYING)){
			state = GameState.ENDED;
			pvp = false;
			CheckRemainingPlayerThread.stop();
			PreventFarAwayPlayerThread.stop();
			PlayersManager.instance().endAllPlayers(cause,winningTeam);
			RestartThread.start();
		}
		
	}

	
	


	
	
	

}
