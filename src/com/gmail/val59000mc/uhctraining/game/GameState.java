package com.gmail.val59000mc.uhctraining.game;

public enum GameState {
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED;
}
