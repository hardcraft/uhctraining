package com.gmail.val59000mc.uhctraining.dependencies;

import java.util.List;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.placeholders.SimplePlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.TeamType;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.gmail.val59000mc.uhctraining.players.UTeam;
import com.google.common.collect.Lists;

public class TeamPlaceholder extends SimplePlaceholder{

	private TeamType type;
	
	public TeamPlaceholder(String pattern, TeamType type) {
		super(pattern);
		this.type = type;
	}
	
	@Override
	public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
		UTeam team = PlayersManager.instance().getTeam(type);
		List<UPlayer> members = team.getMembers();
		List<String> names = Lists.newArrayList();
		for(UPlayer teammate : members){
			names.add(team.getColor()+"- "+teammate.getName());
		}
		String replacement = String.join("<br>", names);
		return original.replaceAll(getPattern(), replacement);
	}

}
