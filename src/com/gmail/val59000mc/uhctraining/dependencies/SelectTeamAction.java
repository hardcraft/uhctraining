package com.gmail.val59000mc.uhctraining.dependencies;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.uhctraining.game.GameManager;
import com.gmail.val59000mc.uhctraining.game.GameState;
import com.gmail.val59000mc.uhctraining.players.PlayersManager;
import com.gmail.val59000mc.uhctraining.players.TeamType;
import com.gmail.val59000mc.uhctraining.players.UPlayer;
import com.gmail.val59000mc.uhctraining.players.UTeam;

public class SelectTeamAction extends Action{
	
	private String team;
	
	public SelectTeamAction(String team){
		this.team = team;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		if(team.equals("leave")){
			leaveTeam(player,sigPlayer);
		}else{
			selectTeam(player,sigPlayer);
		}
		
		
	}

	private void selectTeam(Player player, SigPlayer sigPlayer) {
		
		TeamType type = TeamType.valueOf(team);
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			UPlayer uPlayer = pm.getUPlayer(player);
			
			if(uPlayer != null){
				UTeam oldTeam = uPlayer.getTeam();
				uPlayer.leaveTeam();
				
				UTeam newTeam = PlayersManager.instance().getTeam(type);
				
				if(newTeam.isFull()){
					if(oldTeam != null){
						oldTeam.addPlayer(uPlayer);
					}
					uPlayer.sendI18nMessage("team.full");
					Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					executeNextAction(player, sigPlayer, false);
					return;
				}else{
					newTeam.addPlayer(uPlayer);
					uPlayer.refreshNameTag();
					executeNextAction(player, sigPlayer, true);
					return;
				}
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
	

	private void leaveTeam(Player player, SigPlayer sigPlayer) {
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			UPlayer uPlayer = pm.getUPlayer(player);
			
			if(uPlayer != null){
				uPlayer.leaveTeam();
				uPlayer.refreshNameTag();
				executeNextAction(player, sigPlayer, true);
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
}
