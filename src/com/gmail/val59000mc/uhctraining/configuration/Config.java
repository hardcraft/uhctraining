package com.gmail.val59000mc.uhctraining.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.dependencies.SelectTeamActionParser;
import com.gmail.val59000mc.uhctraining.dependencies.TeamPlaceholder;
import com.gmail.val59000mc.uhctraining.dependencies.VaultManager;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.players.TeamType;

import net.milkbowl.vault.Vault;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isSIGLoaded;
	public static boolean isBountifulApiLoaded;
	
	// World 
	public static String lastWorldName;
	public static String worldName;
	public static boolean isLoadLastWord;

	// Players
	public static int playersToStart;
	public static int countdownToStart;
	public static int maxPlayers;
	public static int playersPerTeam;
	
	// Locations
	public static String lobbyStr;
	public static Location lobby;
	public static List<String> spawnsLocationsStr;
	public static List<Location> spawnsLocations;
	public static String minBoundsStr;
	public static String maxBoundsStr;
	public static LocationBounds bounds;
	public static LocationBounds lobbyBounds;
	public static Location borderCenter;
	public static Double borderStart;
	public static Double borderEnd;
	public static Integer borderTime;
	
	// Bungee
	public static boolean isBungeeEnabled;
	public static String bungeeServer;
	
	// Rewards
	public static double killReward;
	public static double winReward;
	public static double objectiveReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	
	public static void load(){
		
		FileConfiguration cfg = UHCTraining.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");
		
		// World
		lastWorldName = cfg.getString("world.last-world","last_wic_world");
		isLoadLastWord = cfg.getBoolean("world.load-last-world",true);
		worldName = UUID.randomUUID().toString();
		
		// Players
		playersToStart = cfg.getInt("players.min",10);
		playersPerTeam = cfg.getInt("players.per-team",2);
		countdownToStart = cfg.getInt("players.countdown",10);
		maxPlayers = cfg.getInt("players.max",20);

		// Locations
		lobbyStr = cfg.getString("locations.lobby","0 100 0 0 0");
		spawnsLocationsStr = cfg.getStringList("locations.spawns");
		minBoundsStr = cfg.getString("locations.bounds.min","-55 90 55 0 0");
		maxBoundsStr = cfg.getString("locations.bounds.max","55 58 -55 0 0");
		borderStart = cfg.getDouble("locations.border.start",250);
		borderEnd = cfg.getDouble("locations.border.end",50);
		borderTime = cfg.getInt("locations.border.time",200);
		
		// Bungee
		isBungeeEnabled = cfg.getBoolean("bungee.enable",false);
		bungeeServer = cfg.getString("bungee.server","lobby");
		
		// Rewards
		killReward = cfg.getDouble("reward.kill",5);
		winReward = cfg.getDouble("reward.win",50);
		objectiveReward = cfg.getDouble("reward.objective",15);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		// Dependencies
		loadVault();
		loadSIG();
		loadBountifulApi();

		
		// Sig
		if(isSIGLoaded){
			
			SIGApi sig = SIG.getPlugin().getAPI();
			sig.registerActionParser(new SelectTeamActionParser());
			sig.registerPlaceholder(new TeamPlaceholder("{members.RED}",TeamType.RED));
			sig.registerPlaceholder(new TeamPlaceholder("{members.AQUA}",TeamType.AQUA));
			sig.registerPlaceholder(new TeamPlaceholder("{members.BLUE}",TeamType.BLUE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.DARK_PURPLE}",TeamType.DARK_PURPLE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.GOLD}",TeamType.GOLD));
			sig.registerPlaceholder(new TeamPlaceholder("{members.GREEN}",TeamType.GREEN));
			sig.registerPlaceholder(new TeamPlaceholder("{members.LIGHT_PURPLE}",TeamType.LIGHT_PURPLE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.YELLOW}",TeamType.YELLOW));
			
			try {
				sig.registerConfigurationFile(new File(UHCTraining.getPlugin().getDataFolder(), "teams.yml"));
			} catch (InventoryParseException | TriggerParseException e) {
				//
			}
			
		}
		
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}

	private static void loadSIG(){
		Logger.debug("-> Config::loadSIG");
		
		Plugin sig = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if(sig == null || !(sig instanceof SIG)) {
            Logger.warn(I18n.get("config.dependency.sig-not-found"));
        	 isSIGLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.sig-loaded"));
            isSIGLoaded = true;
        }
		Logger.debug("<- Config::loadSIG");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}


	public static void loadWorldLocations() {
		Logger.debug("-> Config::loadWorldLocations");
		FileConfiguration cfg = UHCTraining.getPlugin().getConfig();
		
		World world = Bukkit.getWorld(worldName);
		
		lobby = Parser.parseLocation(world,lobbyStr);
		

		borderCenter = Parser.parseLocation(world,cfg.getString("locations.border.center","0 60 0 0 0"));
		
		spawnsLocations = new ArrayList<Location>();
		for(String locStr : spawnsLocationsStr){
			spawnsLocations.add(Parser.parseLocation(world,locStr));
		}
		Collections.shuffle(spawnsLocations);
		
		bounds = new LocationBounds(Parser.parseLocation(world,minBoundsStr), Parser.parseLocation(world,maxBoundsStr));
		
		lobbyBounds = new LocationBounds(
				Parser.parseLocation(world,cfg.getString("locations.lobby-bounds.min","544 0 67 0 0")), 
				Parser.parseLocation(world,cfg.getString("locations.lobby-bounds.max","668 112 -69 0 0"))
		);
		
		Logger.debug("<- Config::loadWorldLocations");
		
	}
		
		
		
		
}
