package com.gmail.val59000mc.uhctraining.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.uhctraining.UHCTraining;
import com.gmail.val59000mc.uhctraining.i18n.I18n;
import com.gmail.val59000mc.uhctraining.players.UPlayer;

public class PlayerClassManager {
	
	
	private static PlayerClassManager instance;
	private PlayerClass playerClass;

	public static PlayerClassManager instance(){
		if(instance == null){
			instance = new PlayerClassManager();
		}
		
		return instance;
	}

	private PlayerClassManager(){
	}
	
	public static void load(){
		
		PlayerClassManager instance = PlayerClassManager.instance();

		// defense
		instance.playerClass = instance.parseClass();
	
	}
	
	private PlayerClass parseClass(){
		Logger.debug("-> PlayerClass::parseClass");
		
		ConfigurationSection section = UHCTraining.getPlugin().getConfig().getConfigurationSection("stuff");
		
		if(section == null){
			Logger.severeC(I18n.get("parser.playerclass.not-found"));
			return null;
		}
		
		List<ItemStack> swords = parseItemList(section.getStringList("swords"));
		List<ItemStack> bows = parseItemList(section.getStringList("bows"));
		List<ItemStack> potions = parseItemList(section.getStringList("potions"));
		List<ItemStack> helmets = parseItemList(section.getStringList("helmets"));
		List<ItemStack> chestplates = parseItemList(section.getStringList("chestplates"));
		List<ItemStack> leggings = parseItemList(section.getStringList("leggings"));
		List<ItemStack> boots = parseItemList(section.getStringList("boots"));
		List<ItemStack> extras = parseItemList(section.getStringList("extras"));
		
		return new PlayerClass(
				swords, 
				bows,
				potions, 
				helmets, 
				chestplates,
				leggings, 
				boots,
				extras
		);

	}
	
	private List<ItemStack> parseItemList(List<String> itemsStr){
		List<ItemStack> items = new ArrayList<ItemStack>();
		if(itemsStr == null){
			Logger.severeC(I18n.get("parser.playerclass.items-not-found"));
			return null;
		}
		for(String itemStr : itemsStr){
			items.add(Parser.parseItemStack(itemStr));
		}
		return items;
	}

	public void giveClassItems(UPlayer gPlayer) {
		this.playerClass.applyLogic(gPlayer);
	}
}
