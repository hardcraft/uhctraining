package com.gmail.val59000mc.uhctraining.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.uhctraining.players.UPlayer;

public class PlayerClass {
	protected List<ItemStack> swords;
	protected List<ItemStack> bows;
	protected List<ItemStack> potions;
	protected List<ItemStack> helmets;
	protected List<ItemStack> chestplates;
	protected List<ItemStack> leggings;
	protected List<ItemStack> boots;
	protected List<ItemStack> extras;
	
	public PlayerClass(
			List<ItemStack> swords, 
			List<ItemStack> bows, 
			List<ItemStack> potions, 
			List<ItemStack> helmets, 
			List<ItemStack> chestplates, 
			List<ItemStack> leggings, 
			List<ItemStack> boots,
			List<ItemStack> extras) {
		super();
		this.swords = swords;
		this.bows = bows;
		this.potions = potions;
		this.helmets = helmets;
		this.chestplates =chestplates;
		this.leggings = leggings;
		this.boots = boots;
		this.extras = extras;
	}
	
	public void applyLogic(UPlayer uPlayer){
		if(uPlayer.isOnline()){
			Player player = uPlayer.getPlayer();
			player.setMaxHealth(40);
			player.setHealth(40);
			Inventories.clear(player);
			Inventories.setArmor(player, getRandomArmor());
			Inventories.give(player, getRandomItem(swords));
			
			
			int playerBow = calculatePlayerBow(uPlayer.getPlayer());
			if(Randoms.randomInteger(0, 100) <= playerBow){
				Inventories.give(player, getRandomItem(bows));
				Inventories.give(player, new ItemStack(Material.ARROW, 20));
			}
			
			Inventories.give(player, new ItemStack(Material.COOKED_BEEF, 15));
			
			if(Randoms.randomInteger(0, 100) <= 50){
				Inventories.give(player, getRandomItem(potions));
			}
			
			for(int i = 0 ; i < 10 ; i++){
				Inventories.give(player, getRandomItem(extras));
			}

			int lapisCount = calculatePlayerLapis(uPlayer.getPlayer());
			Inventories.give(player, new ItemStack(Material.INK_SACK, lapisCount, (short) 4));
			
			int expLevel = calculatePlayerLevel(uPlayer.getPlayer());
			player.setLevel(expLevel);
		}
	}
	
	private int calculatePlayerLapis(Player player) {
		if(player.hasPermission("hardcraftpvp.uhctraining.lapis.12"))
			return 12;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.11"))
			return 11;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.10"))
			return 10;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.9"))
			return 9;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.8"))
			return 8;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.7"))
			return 7;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.6"))
			return 6;
		else if(player.hasPermission("hardcraftpvp.uhctraining.lapis.5"))
			return 5;
		else
			return 4;
	}

	private int calculatePlayerLevel(Player player) {
		if(player.hasPermission("hardcraftpvp.uhctraining.exp.22"))
			return 22;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.20"))
			return 20;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.18"))
			return 18;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.16"))
			return 16;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.14"))
			return 14;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.12"))
			return 12;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.10"))
			return 10;
		else if(player.hasPermission("hardcraftpvp.uhctraining.exp.8"))
			return 8;
		else
			return 6;
	}
	
	private int calculatePlayerBow(Player player) {
		if(player.hasPermission("hardcraftpvp.uhctraining.bow.70"))
			return 70;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.65"))
			return 65;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.60"))
			return 60;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.55"))
			return 55;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.50"))
			return 50;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.45"))
			return 45;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.40"))
			return 40;
		else if(player.hasPermission("hardcraftpvp.uhctraining.bow.35"))
			return 35;
		else
			return 30;
	}

	private List<ItemStack> getRandomArmor(){
		List<ItemStack> armor = new ArrayList<ItemStack>();
		armor.add(getRandomItem(helmets));
		armor.add(getRandomItem(chestplates));
		armor.add(getRandomItem(leggings));
		armor.add(getRandomItem(boots));
		return armor;
		
	}
	
	protected ItemStack getRandomItem(List<ItemStack> items){
		return ((items == null || items.isEmpty() ) ? null : items.get(Randoms.randomInteger(0, items.size()-1)));
	}
}
