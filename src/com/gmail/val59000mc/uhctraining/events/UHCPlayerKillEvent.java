package com.gmail.val59000mc.uhctraining.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.uhctraining.players.UPlayer;

public final class UHCPlayerKillEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	private UPlayer killer;
	private UPlayer killed;
	
	public UHCPlayerKillEvent(UPlayer killer, UPlayer killed){
		this.killed = killer;
		this.killed = killed;
	}
	
	public UPlayer getKiller(){
		return killer;
	}
	
	public UPlayer getKilled(){
		return killed;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
