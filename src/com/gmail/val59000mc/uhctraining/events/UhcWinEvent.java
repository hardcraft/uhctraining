package com.gmail.val59000mc.uhctraining.events;

import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.uhctraining.players.UPlayer;

public class UhcWinEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	private Set<UPlayer> winners;
	
	public UhcWinEvent(Set<UPlayer> winners){
		this.winners = winners;
	}

	public Set<UPlayer> getWinners(){
		return winners;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

}
